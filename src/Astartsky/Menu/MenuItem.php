<?php
namespace Astartsky\Menu;

class MenuItem implements \Iterator, \Countable
{
    protected $name;
    protected $url;
    protected $items = array();
    protected $routes;
    protected $args;

    /**
     * @param string $route
     * @return bool
     */
    public function isActive($route)
    {
        return in_array($route, $this->routes);
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getArg($key)
    {
        return isset($this->args[$key]) ? $this->args[$key] : null;
    }

    /**
     * @param string $name
     * @param string $url
     * @param string[] $routes
     * @param string[] $args
     */
    public function __construct($name = null, $url = null, $routes = array(), $args = array())
    {
        $this->name = $name;
        $this->url = $url;
        $this->index = 0;
        $this->routes = $routes;
        $this->args = $args;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param MenuItem $item
     */
    public function addItem(MenuItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * Return the current element
     * @return mixed Can return any type.
     */
    public function current()
    {
        return isset($this->items[$this->index]) ? $this->items[$this->index] : null;
    }

    /**
     * Move forward to next element
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        ++$this->index;
    }

    /**
     * Return the key of the current element
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->index;
    }

    /**
     * Checks if current position is valid
     * @return boolean The return value will be casted to boolean and then evaluated.
     */
    public function valid()
    {
        return isset($this->items[$this->index]);
    }

    /**
     * Rewind the Iterator to the first element
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->index = 0;
    }

    /**
     * Count elements of an object
     * @return int The custom count as an integer.
     * The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->items);
    }
}
